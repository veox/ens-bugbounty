# ens-bugbounty

## Intro

By Noel Maersk ([veox][veox]).

These are my source files for the entry to the Ethereum Bug Bounty on
[Ethereum Name Service][ens].

The goal was to:

* demonstrate in a reproducible way the presence of program flaws
  (none have been found);
* showcase unanticipated uses (postponed).

[veox]: https://veox.pw/
[ens]: https://ens.domains


## Process

TODO


## Toolchain

The project uses Python v3.6 and [Populus][populus] v1.6.8.

It is written as a set of test cases.

It doesn't expand ENS' own test cases, since:

* those are written in JavaScript using Truffle, which I'm both
  not very comfortable with;
* the goal was to find a small set flaws, not better demonstrate
  correctness, so could benefit from a different toolchain
  implementation;
* I didn't want to rely on pre-made assumptions and limit my own
  ignorance by using external helper functions.

See [Environment](#environment) for package versions.

[populus]: https://github.com/pipermerriam/populus


## Directory tree

Follows Populus default project structure.

```
.
├── ./contracts
│   ├── ./contracts/AbstractENS.sol
│   ├── ./contracts/ENS.sol
│   └── ./contracts/HashRegistrarSimplified.sol
├── ./populus.json
├── ./README.md
├── ./requirements.txt
└── ./tests
    ├── ./tests/test_ens.py
    └── ./tests/test_hashregsimplified.sol
```

Files in `contracts` are raw downloads from [ethereum/ens][ethereum-ens]
github repo, at commit `1ce01f2`. These were the only ones checked.

Files in `tests` are the actual results of the "bug hunt", as described
in [Process](#process).

[ethereum-ens]: https://github.com/ethereum/ens/tree/1ce01f29d601f843ac308a015c327c702d224cc7/contracts


## Environment

This was done on an Arch Linux (64-bit) system, up-to-date on 2017-04-28.
`geth` v1.6.0 from `community` repo and `solidity` v0.4.8 via `abs` were
used.

For the latter, [the following was done][sol-048] to roll back from
then-latest v0.4.10:

> I was able to downgrade manually by way of `abs` `community/solidity`,
> changing `pkgversion` in its `PKGBUILD` to `0.4.8`, turning off
> `-Werror` in sources' `solc/CMakeLists.txt` (horrible, I know),
> compiling manually (as shown in `PKGBUILD`), then using
> `makepkg --package` to wrap it up and `pacman -U` to install.

The Python part of the environment is documented as `pip`'s
`requirements.txt`.

[sol-048]: https://github.com/pipermerriam/populus/issues/287#issuecomment-298446791


## Installation

To install these on Arch, where CPython 3.6 is currently the default:

``` sh
sudo pacman -S python-virtualenv python-pip

export VIRTUAL_ENV=.virtualenv/ens-bugbounty
mkdir -p $VIRTUAL_ENV
virtualenv $VIRTUAL_ENV
source $VIRTUAL_ENV/bin/activate
pip install -r requirements.txt
```

## Execution

To run the tests:

``` sh
py.test tests/
```
