# Blocking `.eth` registrations for certain names

## Intro

The description is for the [current .eth Registrar][hrs], and touches
on two topics:

* a way of registering a name for the purpose of blocking its use;
* transferring names to the permanent Registrar.

**This is not a bug!** After all, it discusses code that hasn't even
been written yet. :)

A few considerations for the permanent Registrar are presented at the
end.

[hrs]: https://github.com/ethereum/ens/blob/7e377df83f733a4213f123602239fad073bdbaa7/contracts/HashRegistrarSimplified.sol


## Use case ("attack vector")

/u/EggOnAWall writes on [/r/ethereum][eggpost]:

> I'm torn: I want to [bid] on an ENS name. Let's call it
> "Evil-Media-Company". I don't want them to be able to use their name
> in ENS.
>
> If I win the bid I will create a new account, transfer the name, and
> destroy the private key for the new account. Their ENS name will
> forever point into meaningless. Nobody will be able to change it.
>
> ..
>
> edit: this needs some kind of smart contract, since expired ENS
> domains would be released to the wild

[eggpost]: https://www.reddit.com/r/ethereum/comments/6bxam8/state_of_the_ens_week_2_now_with_charts/dhr776r/

To achieve what they describe, no contract is needed, and not even a
name transfer. They can leave the auction unfinalised, and destroy
the bidding account's key instead, after having won the auction.
Creating a separate account in advance for just this purpose is trivial.

(An assumption is made here, that the "name blocking" participant
doesn't mind spending their entire bid to achieve the goal.)


## Post-auction state description

All Registry data for that hash/node will remain pointing to `0x00..00`.

The Registrar will still have the auction results, though. The `entry`'s
`state(hash) == Mode.Owned` prevents subsequent re-auctioning via
`startAuction()` (as it should).

So on a high level, when considered by the Registry-Registrar-Resolver
system of contracts, a name can currently:

* be "owned" temporarily, and transition to "open" on expiry;
* be "open" by default, where the expected transition is to "owned",
  after an auction process;
* be "in limbo", where it is neither truly "owned" nor "open", but
  intentionally left in a transition meta-state.

In the latter case, there's a disparity between who the Registrar thinks
the owner is (the `owner` of the winning `Deed`), and who the Registry
thinks the owner is (`0x00..00`).

It will be possible to transfer this state of affairs to the permanent
registrar via `transferRegistrars()`.

(NB: Name expiry is currently implemented wholesale, as a community
agreement to transition to a permanent registrar, enforceable by the
MultiSig participants holding ownership of the `.eth` Registrar's
`rootNode` in the Registry.)

All of this is due to `Mode.OwnedButNotFinalised` (or the like) not
being present. In other words, a certain auction state can be seen as
collated into a different state, `Mode.Owned`.


## Considerations for the transfer process

The permanent Registrar will probably have some form of `expire()`,
similar to current `releaseDeed()`, but with an expiration date check
instead of an owner check (via `onlyOwner` modifier). That should allow
anyone to return a name to the "open" pool, if it's not paid for in
time.

If this function checks for anything but the `Deed`'s `creationDate`, or
the `entry`'s `registrationDate`, there may be unexpected results.

To transfer an `entry` from the temporary to the permanent Registrar,
the latter's `acceptRegistrarTransfer()` will be called, from the
current one's `transferRegistrar()`. Its signature is:

``` solidity
function acceptRegistrarTransfer(bytes32 hash, Deed deed, uint registrationDate);
```

Note that `h.value`, `h.highestBid` and `state(_hash)` are not passed,
so they will likely have to be retrieved separately from the temporary
Registrar via `entries()` if required. (Ironically, if that is
performed, then passing `deed` and `registrationDate` could have been
skipped.)

It's nice that `Mode` (via `state(_hash)`) is not passed, thus
avoiding possible quirks with the underlying representation of
`enum`. In fact, from the temporary Registrar's point of view, in can
only be `Mode.Owned` - guaranteed by the `onlyOwner` modifier for
`transferRegistrar()`.


## How this could become an issue

If the "name blocking" use, as outlined in the beginning, sees much
traction, but is considered a mis-use, then a mechanism can be provided
to guard against it.

For example, anyone could be allowed to "cancel" an auction if it's
not finalised in time (e.g. a week) after auction end, possibly with
the lazy winner [paying the same participation fee][nohodl] as
everybody else did.

This would:

* encourage finalising auctions;
* prevent discrepancies between the Registry's and the Registrar's
  view on hash ownership,
* possibly prevent unexpected results when using the Resolver, e.g.
  when retrieving the `owner` of a seemingly-registered node;
* require "name blocking" participants to be explicit about their
  intent, instead of using a technical quirk.

Depending on how such a state of affairs is detected by the permanent
Registrar - a separate `Mode` or a modifier check, - unfinalised entries
that have been transferred between registrars might present an issue.

(NB: Something similar may also be required for `expire()`, e.g. a
`Mode.Expired` or another modifier/check, but that's slightly OT.)

[nohodl]: https://github.com/ethereum/ens/issues/189


## Conclusions

There will necessarily be differences between the temporary and
permanent `.eth` Registrars.

It is possible to have a "correct" entry in the current Registrar that
_may_ not be "correct" in the next one, depending on implementation
details.

It is important that these are identified, considered, and checked for
in advance.
