# Many snippets from https://populus.readthedocs.io/en/latest/deploy.html

#import pprint
#import time

import rlp

def check_succesful_tx(chain, txid: str, timeout = 180) -> dict:
    '''See if transaction went through (Solidity code did not throw).

    :return: Transaction receipt
    '''

    # http://ethereum.stackexchange.com/q/6007/620
    receipt = chain.wait.for_receipt(txid, timeout = timeout)
    tx = chain.web3.eth.getTransaction(txid)

    #pprint.pprint(chain.web3.eth.getTransaction(txid))
    #pprint.pprint(receipt)

    # EVM has only one error mode and it's consume all gas
    assert tx['gas'] > receipt['gasUsed']
    return receipt


def deploy_ens(chain):
    '''Helper to deploy ENS.

    :return: ENS contract address'''

    ens, txhash = chain.provider.get_or_deploy_contract('ENS')
    print('Deploying ENS, tx hash is ', txhash)
    
    receipt = check_succesful_tx(chain, txhash)
    ensaddr = receipt['contractAddress']
    print('ENS contract address is ', ensaddr)

    return ensaddr


def deploy_registrar(chain):
    '''Helper to deploy Registrar contract.

    Checks if ENS is already available, and deploys one if not.

    Then deploys Registrar.

    :return: Registrar contract address'''

    # The address who will be the owner of the contracts
    coinbase = chain.web3.eth.coinbase
    assert coinbase, 'Make sure your node has coinbase account created.'

    ens, txhash = chain.provider.get_or_deploy_contract('ENS')
    if not txhash:
        print('ENS already available at', ens.address)
    else:
        print('Deploying ENS prior to deploying Registrar, tx hash is', txhash)
        receipt = check_succesful_tx(chain, txhash)
        ensaddr = receipt['contractAddress']
        print('ENS contract address is ', ensaddr)

    # ENS sets `owner` of 0x0 to `msg.sender` of deployment tx
    assert ens.call().owner(b'') == coinbase

    # ensutils.js says this is namehash('eth'); its `owner` in the ENS registry
    # points to the following address, which is indeed the .eth Registrar:
    # https://etherscan.io/address/0x6090a6e47849629b7245dfa1ca21d94cd15878ef
    # FIXME: is this correct, or is the encoding wrong?
    rootnode = bytes.fromhex('93cdeb708b7545dc668eb9280176169d1c33cfd8ed6f04690a0bcc88a93fc4ae')
    # can set to `0` to start immediately
    startdate = 1493895600 # int(time.time())

    factory = chain.get_contract_factory('Registrar')
    txhash = factory.deploy(
        transaction = {'from': coinbase},
        args = [ens.address, rootnode, startdate])
    print('Deploying Registrar, tx hash is', txhash)

    receipt = check_succesful_tx(chain, txhash)
    regaddr = receipt['contractAddress']
    print('Registrar contract address is', regaddr)

    # HACK: for some reason, the address is not registered with Populus - do it manually
    chain.registrar.set_contract_address('Registrar', regaddr)
    
    return regaddr


def test_hrs(chain):
    '''Deployment of Registrar (with arguments).'''

    print('Web3 provider is', chain.web3.currentProvider)

    # spammy
    #pprint.pprint(chain.provider.get_all_contract_data())
    
    # separate deployment optional, but done here
    ensaddr = deploy_ens(chain)
    assert ensaddr, 'ENS address is False!'

    # deploys ENS, too, if that unavailable
    regaddr = deploy_registrar(chain)
    assert regaddr, 'Registrar address is False!'
    
    assert chain.provider.is_contract_available('ENS'), 'Populus says ENS is not available!'
    assert chain.provider.is_contract_available('Registrar'), 'Populus says Registrar is not available!'

    return


def namehash(fun, fullname: str):
    '''Helper for name-to-hash conversion.

    The name shall be a string, dot-delimited between namespaces.

    NOTE: Reimplements function in simplehashregistrar_test.js. Can't yet
    find respective part in actual contracts. :/

    :return: hash expected by contract'''

    # FIXME STUB WORKHERE
    rootnodehexstring = '93cdeb708b7545dc668eb9280176169d1c33cfd8ed6f04690a0bcc88a93fc4ae'
    #return rlp.utils.decode_hex(rootnodehexstring)
    return bytes.fromhex(rootnodehexstring)


def test_getAllowedTime(chain):
    '''Calling getAllowedTime().

    Some checks are the same as in 'launch starts slow with scheduled availability' test of
    https://github.com/ethereum/ens/blob/master/test/simplehashregistrar_test.js
    '''

    # This is a clean test chain run.
    assert not chain.provider.is_contract_available('ENS'), 'ENS unexpectedly available!'
    assert not chain.provider.is_contract_available('Registrar'), 'Registrar unexpectedly available!'

    deploy_registrar(chain)
    
    #ens = chain.provider.get_contract('ENS')
    reg = chain.provider.get_contract('Registrar')

    # time when auctioning starts
    start = reg.call().registryStarted()
    # duration of "arrested launch" period
    launchperiod = reg.call().launchLength()
    print('Start time:', start, 'Launch period:', launchperiod)
    print('Launch end:', start + launchperiod)
    
    # auction starts immediately for
    # 0x0000000000000000000000000000000000000000000000000000000000000000
    # TODO: check if an auction for this one can be started! ;) (probably a separate test)
    # TODO: check also auctions for {namehash,keccak256}('{eth,ethereum,ethereum.eth}')
    assert start == reg.call().getAllowedTime(b'')
    assert start == reg.call().getAllowedTime(bytes(32))

    # Labels that hash to this hex number will "open" at half the launch period.
    # (also demonstrates which one)
    cookies = ['80',
               '8000000000000000000000000000000000000000000000000000000000000000',
               '0000000000000000000000000000000000000000000000000000000000000080',]
    expecting = [True, True, False]
    responses = []
    for cookie in cookies:
        bytes_ = bytes.fromhex(cookie)
        contractsays = reg.call().getAllowedTime(bytes_)
        if contractsays == start + launchperiod/2:
            print('This one!     Contract says:', contractsays, 'for cookie (hex):', cookie)
            responses.append(True)
        else:
            print('Not this one. Contract says:', contractsays, 'for cookie (hex):', cookie)
            responses.append(False)
    assert expecting == responses

    # TODO: auctions for {namehash,keccak256}('{eth,ethereum,ethereum.eth}')
    # > loadScript('ensutils.js')
    # > web3.sha3('eth')
    # "0x4f5b812789fc606be1b3b16908db13fc7a9adf7ca72641f84d75b47069d3d7f0"
    # > web3.sha3('ethereum.eth')
    # "0x71162a7eb6f243f6a82d16d11aa27f63fa2694ac24093d5119f8f2c6221f7b02"
    # > web3.sha3('ethereum')
    # "0x541111248b45b7a8dc3f5579f630e74cb01456ea6ac067d3f4d793245a255155"
    # > namehash('eth')
    # "0x93cdeb708b7545dc668eb9280176169d1c33cfd8ed6f04690a0bcc88a93fc4ae"
    # > namehash('ethereum.eth')
    # "0x78c5b99cf4668cf6da387866de4331c78b75b7db0087988c552f73e1714447b9"
    # > namehash('ethereum')
    # "0xee859a6e5a5c99ad5b70014b9c53f94c853d066f176920a8842edeba4a2b9403"
    # > web3.sha3('')
    # "0xc5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470"
    # > namehash('')
    # "0x0000000000000000000000000000000000000000000000000000000000000000"

    # 'eth'
    h = namehash(chain.web3.sha3, 'eth')
    print(h)
    print(reg.call().rootNode())
    assert h == rlp.utils.decode_hex(reg.call().rootNode())
    
    # # 'ethereum.geth'
    # h = namehash(chain.web3.sha3, 'ethereum.eth')
    # assert h == '78c5b99cf4668cf6da387866de4331c78b75b7db0087988c552f73e1714447b9'
    # timestamp = reg.call().getAllowedTime(bytes.fromhex(h))

    # # TODO: check how this is guaranteed, especially overflows
    # assert timestamp >= start

    # # `ethRegistrar.getAllowedTime('ethereum')` in `geth console` gives `1495813094`
    # assert timestamp == 1495813094

    return
