import pprint

def test_ens(chain):
    '''Deployment (constructor has no arguments).'''
    ens, _ = chain.provider.get_or_deploy_contract('ENS')

    assert ens.call().owner(b'')     == chain.web3.eth.coinbase
    assert ens.call().owner(b'\x00') == chain.web3.eth.coinbase
    assert ens.call().owner('bogus') == '0x0000000000000000000000000000000000000000'

    return

def test_transact(chain):
    '''Transaction test, e.g. setting resolver.'''
    ens, _ = chain.provider.get_or_deploy_contract('ENS')

    resaddr = '0x0000000000000000000000000000000000031337'
    txhash = ens.transact().setResolver(b'\x00', resaddr)
    chain.wait.for_receipt(txhash)

    assert ens.call().resolver(b'\x00') == resaddr

    return

def test_subnode(chain):
    '''setSubnodeOwner() calls are on externally encoded bytes32 hashes.

    They also don't check if the subnode-to-change has an owner set already, whether it's expired
    or not, - anything but the owner of the parent node making the call.'''

    ens, _ = chain.provider.get_or_deploy_contract('ENS')

    owner = ens.call().owner(b'\x00')
    assert owner == chain.web3.eth.coinbase

    # setting owner of node's 0x(00) subnode 0x(00)
    txhash = ens.transact().setSubnodeOwner(b'\x00', b'\x00', owner)
    txreceipt = chain.wait.for_receipt(txhash)

    # will be toying with this subnode
    thishash = chain.web3.sha3('00'*64, encoding='hex')
    assert thishash == '0xad3228b676f7d3cd4284a5443f17f1962b36e491b30a40b2405849e597ba5fb5'
    
    answer = ens.call().owner(bytes.fromhex(thishash[2:]))
    assert answer == owner
    
    # will be re-setting with this one
    newowner = '0x0000000000000000000000000000000000031337'

    # re-setting owner of node's 0x(00) subnode 0x(00)
    txhash = ens.transact().setSubnodeOwner(b'\x00', b'\x00', newowner)
    txreceipt = chain.wait.for_receipt(txhash)

    #pprint.pprint(chain.web3.eth.getTransaction(txhash))
    #pprint.pprint(txreceipt)

    # NOTE: Why doesn't the NewOwner event also log the "final node" changed?..
    node = '0000000000000000000000000000000000000000000000000000000000000000'
    label = '0000000000000000000000000000000000000000000000000000000000000000'
    
    thishash = chain.web3.sha3(node+label, encoding='hex')
    assert thishash == '0xad3228b676f7d3cd4284a5443f17f1962b36e491b30a40b2405849e597ba5fb5'
    
    answer = ens.call().owner(bytes.fromhex(thishash[2:]))
    assert answer == newowner

    # debug-print (assert supremacy)
    print("b'':         "   + chain.web3.sha3(b''))
    print("b'':         "   + chain.web3.sha3(b'', encoding='bytes'))
    print("b'\\x00':     "  + chain.web3.sha3(b'\x00', encoding='bytes'))
    print("b'\\x00\\x00': " + chain.web3.sha3(b'\x00\x00', encoding='bytes'))
    print("bytes(32):   "   + chain.web3.sha3(bytes(32), encoding='bytes'))
    print("bytes(32)*2: "   + chain.web3.sha3(bytes(32)*2, encoding='bytes'))
    #assert False

    return
